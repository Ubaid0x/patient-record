import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Actions from "../../store/action/actionCenter";
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import frontScreen from '../../images/frontScreen.png';
import UserImage from '../../images/user-circle-solid.svg';
import TextField from '@material-ui/core/TextField';
import  Button from '@material-ui/core/Button';
import green from '@material-ui/core/colors/green';
import './signup.css';

const styles = theme => ({
  Body: {
    backgroundColor: '#eff0f4'
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  resize:{
    fontSize:'1.5em',
  },
});

const theme = createMuiTheme({
  palette: {
    primary: green,
  },
});

class Signup extends Component {
  constructor(){
    super()
    this.state = {
      username: '',
      password: '',
      confirmPassword: '', 
      email: '',
    }
  }

  componentWillReceiveProps(user) {
    if (user) {
      this.props.history.push("/Dashboard");
    }
  }
  
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  Validate = (Data)=>{
    var signupData = {}
    if(this.state.password === this.state.confirmPassword)
    {
      signupData = {
        username: this.state.username,
        email: this.state.email,
        password: this.state.password
      } ;
      this.props.Signup(signupData);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div style={{backgroundColor: '#eff0f4'}}>
      <div>
        <div className="Head">
          <img src={frontScreen} alt="Splash Screen" className="headImage" />
        </div>
        <div className="Body" elevation={1}>
          <img src={UserImage} alt="user Image" className="userImage" />
          {/* <i className="fa fa-hospital-alt fa-5x userImage"></i> */}

          <h2> Sign Up </h2>
          <div className="Textfield">
            <i className="fa fa-user fa-2x" ></i>
            <MuiThemeProvider theme={theme} >
              <TextField style={{width:'40%',marginRight: '1%'}}
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
                value= {this.state.username}
                onChange={this.handleChange('username')}
                className={classes.margin}
                label="Username"
                id="mui-theme-provider-input"
              /> 
            </MuiThemeProvider>
            <i className="fa fa-envelope fa-2x"></i>
            <MuiThemeProvider theme={theme} >
              <TextField style={{width:'40%',}}
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
                value= {this.state.email}
                onChange={this.handleChange('email')}
                className={classes.margin}
                label="Email"
                id="mui-theme-provider-email"
                type="email"
              />
            </MuiThemeProvider>
          </div>
          <div className="Textfield">
            <i className="fa fa-lock fa-2x" > </i>
            <MuiThemeProvider theme={theme} >
              <TextField style={{width:'40%',marginRight: '2%'}}
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
                value= {this.state.password}
                onChange={this.handleChange('password')}
                className={classes.margin}
                label="Password"
                autoFocus={true}
                type="password"
                id="mui-theme-provider-pass"
              />
            </MuiThemeProvider>
            <i className="fa fa-lock fa-2x" > </i>
            <MuiThemeProvider theme={theme} >
              <TextField style={{width:'40%',marginRight: '1%'}}
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
                value= {this.state.confirmPassword}
                onChange={this.handleChange('confirmPassword')}
                className={classes.margin}
                label="Confirm Password"
                autoFocus={true}
                type="password"
                id="mui-theme-provider-Cfnpass"
              />
            </MuiThemeProvider>
          </div>
          <div className="LoginBtn">
            <Button onClick={()=>{this.Validate(this.state)}}>
              Signup
            </Button>
          </div>
          <div className="SignupBtn">
            <Button onClick={() => this.props.history.push('/')}> Login </Button>
          </div>
        </div>
      </div>
      </div>
    );
  }
}
Signup.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state){
  return {
    user: state.doctorReducers
  }
}
function mapDispatchToProps(dispatch){
  return {
    Signup : function(signupData){
      return dispatch(Actions.signupAct(signupData))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Signup));