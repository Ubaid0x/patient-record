import React, { Component } from 'react';
import {withStyles, MuiThemeProvider, createMuiTheme }  from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import green from '@material-ui/core/colors/green';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import Head from './Head';
import {connect} from 'react-redux';
import Actions from "../store/action/actionCenter";
import CircularProgress from '@material-ui/core/CircularProgress';
import '../App.css';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#74dbef',
    color: theme.palette.common.white,
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Arial Black'
  },
  body: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'Arial Black'
  },
}))(TableCell);

const styles = theme => ({
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
  },
  root: {
    flexGrow: 1,
  },
  primary: {},
  icon: {},
  progress: {
    margin: theme.spacing.unit * 2,
  },
}
});

const theme = createMuiTheme({
  palette: {
    primary: green,
  },
});

class Dashboard extends Component {
  constructor(){
    super();
    this.state = {
      title : 'Patient',
      patients: [],
      // copyPatient: [this.state.patients],
      open: false,
      scroll: 'paper',
      firstname: '',
      lastname: '',
      appointmentDate: '',
      contact: null,
      email : '',
      category: '',
      id: null,
    }
  }
  componentWillMount() {
    this.props.getAllPatientsData()
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps);
    if(nextProps.patients)
    {
      this.setState({
        patients: nextProps.patients
      })
    }
  }

  handleClickOpen = (patientData) => {
    this.setState({ 
      open: true, 
      id: patientData._id, 
      firstname: patientData.firstname, 
      lastname: patientData.lastname, 
      category: patientData.category,
      appointmentDate: patientData.appointmentDate, 
      contact: patientData.contact,
      email: patientData.email,
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = (name ,event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  UpdatePatient = (event) => {
    event.preventDefault();
    if(this.state.firstname || this.state.lastname || this.state.category 
      || this.state.contact || this.state.email || this.state.appointmentDate) {
        const record = {
          id: this.state.id,
          firstname: this.state.firstname, 
          lastname: this.state.lastname, 
          category: this.state.category,
          appointmentDate: this.state.appointmentDate, 
          contact: this.state.contact,
          email: this.state.email,
        }
        this.props.UpdatePatientRecord(record);
        this.setState({ open: false,});
        
    }
    else {
      console.log('Already Updated');
    }
  }

  onDelete = (patient) => {
    console.log('Delete ', patient)
    this.props.deletePatient(patient);
  }

  render() {
    const { classes } = this.props;
    const { patients } = this.state;
    return (
      <div>
          <Head title={this.state.title }/>
          <Paper className={classes.root} className="setTable">
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <CustomTableCell component="th">Patient Name</CustomTableCell>
                    <CustomTableCell component="th">Appointment Date</CustomTableCell>
                    <CustomTableCell component="th">Contact No</CustomTableCell>
                    <CustomTableCell component="th">Edit</CustomTableCell>
                    <CustomTableCell component="th">Delete</CustomTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                    { patients.length !== 0 ? 
                      patients.map((patient, index) => {
                        return (
                          <TableRow key={index} className={classes.row}>
                            <CustomTableCell component="th" scope="row">{patient.firstname +' '+ patient.lastname }</CustomTableCell>
                            <CustomTableCell>{patient.appointmentDate}</CustomTableCell>
                            <CustomTableCell>{patient.contact}</CustomTableCell>
                            <CustomTableCell>
                              <Button
                                mini
                                variant="fab"
                                color="primary"
                                aria-label="edit"
                                onClick={() => this.handleClickOpen(patient)}
                              >
                                <EditIcon />
                              </Button>
                            </CustomTableCell>
                            <CustomTableCell>
                              <Button
                                mini
                                variant="fab"
                                color="secondary"
                                aria-label="edit"
                                onClick={() => this.onDelete(patient)}
                              >
                                <DeleteIcon />
                              </Button>
                            </CustomTableCell>
                          </TableRow>
                        );
                    }) : 
                    (
                      <div style= {{textAlign: 'center'}}>
                        <CircularProgress className={classes.progress} style= {{textAlign: 'center'}} variant="static" value={50} />
                      </div>
                    )
                  }
                </TableBody> 
              </Table>
            </Paper>
            <Dialog fullWidth
              open={this.state.open}
              onClose={this.handleClose}
              scroll={this.state.scroll}
              aria-labelledby="scroll-dialog-title" >
              <DialogTitle id="scroll-dialog-title" 
              style={{textAlign: 'center', backgroundColor: 'lightseagreen', fontWeight: 'bold'}}>Update Patient</DialogTitle>
              <Divider />
              <DialogContent>
                <DialogContentText>
                  <div style={{paddingTop: '1%'}}>
                    <p>Select Category</p>
                    <FormControl className={classes.formControl} style = {{width: '90%',}}>
                        <Select
                          value={this.state.category}
                          onChange={(e) => this.handleChange('category', e)}
                          name="Category"
                          className={classes.selectEmpty} >
                          <MenuItem value='Mental Health'>Mental Health</MenuItem>
                          <MenuItem value='MetaBolic And Obacity'>MetaBolic And Obacity</MenuItem>
                          <MenuItem value='Yoga'>Yoga</MenuItem>
                        </Select>
                    </FormControl>
                  </div>
                  <div style={{paddingTop: '1%'}}>
                    <p>Firstname</p>
                    <MuiThemeProvider theme={theme}>
                      <TextField
                        InputProps={{
                          classes: {
                            input: classes.resize,
                          },
                        }}
                        className={classes.margin}
                        onChange= {(e) => this.handleChange('firstname', e)}
                        style= {{width: '90%'}}
                        value= {this.state.firstname}
                        id="mui-theme-provider-input"
                      />
                  </MuiThemeProvider>
                  </div>
                  <div style={{paddingTop: '1%'}}>
                    <p>Lastname</p>
                    <MuiThemeProvider theme={theme}>
                      <TextField
                        InputProps={{
                          classes: {
                            input: classes.resize,
                          },
                        }}
                        className={classes.margin}
                        onChange= {(e) => this.handleChange('lastname', e)}
                        style= {{width: '90%'}}
                        value= {this.state.lastname}
                        id="mui-theme-provider-input"
                      />
                    </MuiThemeProvider>
                  </div>
                  <div style={{paddingTop: '1%'}}>
                    <p>Appointment Date</p>
                    <TextField
                      id="date"
                      type="date"
                      Value= {this.state.appointmentDate}
                      onChange= {(e) => this.handleChange('appointmentDate', e)}
                      style = {{width: '90%'}}
                      InputProps={{
                        classes: {
                          input: classes.resize,
                        },
                      }}
                      className={classes.textField}
                    />
                  </div>
                  <div style={{paddingTop: '1%'}}>
                    <p>Email</p>
                    <MuiThemeProvider theme={theme}>
                      <TextField
                      InputProps={{
                        classes: {
                          input: classes.resize,
                        },
                      }}
                      value= {this.state.email}
                      onChange= {(e) => this.handleChange('email', e)}
                      className={classes.margin}
                      style = {{width: '90%',}}
                      id="mui-theme-provider-input"
                      type="email" />
                      </MuiThemeProvider>
                  </div>
                  <div style={{paddingTop: '1%'}}>
                    <p>Contact Number</p>
                    <MuiThemeProvider theme={theme}>
                      <TextField
                      InputProps={{
                        classes: {
                          input: classes.resize,
                        },
                      }}
                      className={classes.margin}
                      value= {this.state.contact}
                      onChange= {(e) => this.handleChange('contact', e)}
                      style = {{width: '90%'}}
                      id="mui-theme-provider-input"
                      type="number" />
                    </MuiThemeProvider>
                  </div>
                </DialogContentText>
              </DialogContent>
              <DialogActions style={{backgroundColor: 'lightseagreen', justifyContent: 'center'}}>
                <Button onClick={(e) => this.UpdatePatient(e)} style={{color: 'white'}} >
                  Update
                </Button>
                <Button onClick={(e) => this.handleClose(e)} style={{color: 'white'}} >
                  Cancel
                </Button>
              </DialogActions>
            </Dialog>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    patients: state.patientReducers.patient 
  }
}
function mapDispatchToProps(dispatch) {
  return {
    getAllPatientsData: function() {
      return dispatch(Actions.getAllPatientsDataAct());
    },
    UpdatePatientRecord: function(record) {
      return dispatch(Actions.updatePatientAct(record));
    },
    deletePatient: function(patient) {
      return dispatch(Actions.deletePatientAct(patient));
    }
  }
}
Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Dashboard));


