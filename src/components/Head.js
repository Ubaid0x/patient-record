import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {withStyles}  from '@material-ui/core/styles';
import MenuList from '@material-ui/core/MenuList';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import '../App.css';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  root: {
    flexGrow: 1,
  },
  menuItem: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  primary: {},
  icon: {},
});

class Head extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Paper className="paper" style={{height: '100% !important'}} >
          <Paper className="paperHead"> 
            <h2>Patient Tracker</h2>
          </Paper>
          <MenuList>
            <MenuItem className={classes.menuItem} >
              <ListItemText classes={{ primary: classes.primary }}
               inset primary="Patient Information" onClick= {() => {this.props.history.push('/Dashboard')}} />
            </MenuItem>
            <MenuItem className={classes.menuItem} >
              <ListItemText classes={{ primary: classes.primary }} 
              inset primary="Add Patient" onClick= {() => {this.props.history.push('/AddPatient')}} />
            </MenuItem>
          </MenuList>
        </Paper>
        <div className={classes.root} className="appBar">
          <AppBar position="static" color="default">
            <Toolbar>
              <Typography variant="title" color="inherit">
                <h2 style={{lineHieght: '5em', fontWeight: 'bold' }} > {this.props.title} </h2>
              </Typography>
              <Typography variant="title" color="inherit">
                
              </Typography>
              <Typography variant="title" color="inherit" className="LogoutButton" >
                <i className="fa fa-search"></i>                
                <TextField variant="contained"
                  id="search"
                  label="Search"
                  type="search"
                  className={classes.textField}
                  // margin="normal"
                  style={{lineHieght: '5em', fontWeight: 'bold', paddingBottom: 10,}}
                />
                <Button variant="contained" color="secondary" className={classes.button} 
                  onClick={() => {this.props.history.push('/')}} >
                  Log out
                </Button>
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
      </div>
    );
  }
}
Head.propTypes = {
  classes: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    patients: state.patientReducers.patient 
  }
}
function mapDispatchToProps(dispatch) {

}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withStyles(styles)(Head)));


