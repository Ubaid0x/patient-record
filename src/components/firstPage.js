import React, { Component } from 'react';
import '../App.css';
import frontScreen from '../images/frontScreen.png';


class FirstPage extends Component {
  render() {
    return (
      <div className="App">
        <div className="ImageDiv"> 
            <img src={frontScreen} alt="Splash Screen" />
        </div>
      </div>
    );
  }
}
export default (FirstPage);