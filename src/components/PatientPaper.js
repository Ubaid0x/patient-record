import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import {withStyles}  from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  textField: {
    flexBasis: 200,
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  formControl: {
    margin: theme.spacing.unit * 3,
    flexDirection: 'row',
  },
  resize:{
    fontSize:'1.5em',
  },
});

class PatientPaper extends Component {
  constructor(){
    super();
    this.state = {
      account: ''
    }
  }

  HandleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
  state = {
    value: 'female',
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

render() { 
  const { classes } = this.props
  return (
    <div>
      <Paper className={classes.root} elevation={1} className="bodyPaper">
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}}>
          <div className={classes.margin} className="inputSet">
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
                <i class="fa fa-user"></i>
              </Grid>
              <Grid item >
                <TextField id="input-with-icon-grid" label="FirstName" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
              <Grid item>
                <i class="fa fa-user"></i>
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="LastName" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
              <Grid item>
                <FormControl component="form" required error className={classes.formControl} style={{width: '100%'}}>
                    <RadioGroup
                      style={{display: 'inline-block'}}
                      aria-label="gender"
                      name="gender2"
                      className={classes.group} 
                      value={this.state.value}
                      onChange={this.handleChange} >
                      <FormLabel component="label" style={{color: 'black',fontSize: '1em'}}>Gender</FormLabel>
                      <FormControlLabel value="female" control={<Radio color="primary" />} label="Female" />
                      <FormControlLabel value="male" control={<Radio color="primary" />} label="Male" />
                    </RadioGroup>
                </FormControl>
              </Grid>
            </Grid>
          </div> 
        </Typography>
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}}>
          <div className={classes.margin}>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
              <i class="fa fa-envelope"></i>
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="Email Address" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
              <Grid item>
                <AccountCircle />
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="Age" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
            </Grid>
          </div>
        </Typography>
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}}>
          <div className={classes.margin}>
            <Grid container spacing={8} alignItems="flex-start">
              <Grid item>
              <i class="fa fa-user"></i>
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
            </Grid>
          </div>
        </Typography>
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}}>
          <div className={classes.margin}>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
                <AccountCircle />
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="With a grid" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
            </Grid>
          </div>
        </Typography>
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}} className="TextFieldPaper" InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}>
          <div className={classes.margin}>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
                <AccountCircle />
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="With a grid" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
            </Grid>
          </div>
        </Typography>
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}} className="TextFieldPaper" >
          <div className={classes.margin}>
            <FormControl fullWidth className={classes.margin}>
            <i class="fa fa-home"></i>
              <Input
                id="adornment-amount"
                value={this.state.amount}
                placeholder = "Address"
                onChange={this.HandleChange('amount')}
              />
            </FormControl>
          </div>
        </Typography>
        <Typography variant="headline" component="h3" style={{lineHeight: '3em', marginLeft: '2em'}} className="TextFieldPaper">
          <div className={classes.margin}>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
                <AccountCircle />
              </Grid>
              <Grid item>
                <TextField id="input-with-icon-grid" label="City" style={{paddingRight: '10em', width: '130%'}} InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}/>
              </Grid>
            </Grid>
          </div>
        </Typography>
    </Paper>
    </div>
  );
}
}
PatientPaper.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(PatientPaper);