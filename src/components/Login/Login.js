import React, { Component } from 'react';
import './login.css';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Actions from "../../store/action/actionCenter";
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import frontScreen from '../../images/frontScreen.png';
import UserImage from '../../images/user-circle-solid.svg';
import TextField from '@material-ui/core/TextField';
import  Button from '@material-ui/core/Button';
import green from '@material-ui/core/colors/green';


const styles = theme => ({
  Body: {
    backgroundColor: '#eff0f4'
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  resize:{
    fontSize:'1em',
  },
});

const theme = createMuiTheme({
  palette: {
    primary: green,
  },
});

class Login extends Component {
  
  constructor(){
    super()
    this.state = {
      username : '',
      password : '',
      open: false,
      vertical: 'bottom',
      horizontal: 'right',
    }
  }

  componentWillReceiveProps(user) {
    if (user) {
      this.props.history.push("/Dashboard");
    }
  }

  handleClick = state => () => {
    this.setState({ open: true, ...state });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  Validate = (LoginData)=>{
    this.props.Login(LoginData);
  }

  render() {
    const { classes } = this.props;
    const {username, password, vertical, horizontal, open } = this.state;
    return (
      <div style={{backgroundColor: '#eff0f4'}}>
      <div>
        <div className="Head">
          <img src={frontScreen} alt="Splash Screen" className="headImage" />
        </div>
        <div className="Body" elevation={1}>
          <img src={UserImage} alt="user Image" className="userImage" />
          {/* <i className="fa fa-user-circle fa-5x"></i> */}
          <h2> Login </h2>
          <div className="Textfield">
            <i className="fa fa-user fa-2x" style={{padding: '1em'}}></i>
            <MuiThemeProvider theme={theme} >
              <TextField style={{width:'70%'}}
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
                value= {this.state.username}
                onChange={this.handleChange('username')}
                className={classes.margin}
                label="Username"
                id="mui-theme-provider-input"
              />
            </MuiThemeProvider>
          </div>
          <div className="Textfield">
          <i className="fa fa-lock fa-2x" style={{padding: '1em'}}> </i>
            <MuiThemeProvider theme={theme} >
              <TextField style={{width:'70%'}}
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
                value= {this.state.password}
                onChange={this.handleChange('password')}
                className={classes.margin}
                label="Password"
                autoFocus={true}
                type="password"
                id="mui-theme-provider-pass"
              />
            </MuiThemeProvider>
          </div>
          <div className="LoginBtn">
            <Button onClick={()=>{this.Validate(this.state)}}>
              LOGIN
            </Button>
          </div>
          <div className="SignupBtn">
            <h3> Do Not Have An Account Click </h3>
            <Button onClick={() => this.props.history.push('/Signup')}> Register </Button>
          </div>
        </div>
      </div>
        <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            open={open}
            onClose={this.handleClose}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">Login Successfully!!!!!</span>}
          />
    </div>
  );}
}
Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state){
  return{
    user: state.doctorReducers 
  }
}
function mapDispatchToProps(dispatch){
  return {
    Login : function(LoginData){
      return dispatch(Actions.loginAct(LoginData))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login));