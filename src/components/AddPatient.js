import React, { Component } from 'react';
import '../App.css';
import PropTypes from 'prop-types';
import {withStyles, MuiThemeProvider, createMuiTheme }  from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {classNames, classes} from 'classnames';
import Head from './Head';
import {connect} from 'react-redux';
import Actions from "../store/action/actionCenter";
  
const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    margin: {
      margin: theme.spacing.unit,
    },
    cssLabel: {
      '&$cssFocused': {
        color: purple[500],
      },
    },
    cssFocused: {},
    cssUnderline: {
      '&:after': {
        borderBottomColor: purple[500],
      },
    },
    menu: {
    width: 200,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
  resize:{
    fontSize:'1.5em',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

const theme = createMuiTheme({
    palette: {
      primary: green,
    },
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class AddPatient extends Component {
  constructor(){
    super();
    this.state = {
      title : 'Add Patient',
      activeStep: 0,
      value: 'female',
      firstname: '',
      lastname: '',
      completed: {},
      admissionDate: '',
      appointmentDate: '',
      contact: null,
      email : '',
      dateOfBirth: '',
      category: '',
      open: false,
    }
  }

  handleChange = (name, event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  radioHandleChange = (event) => {
    this.setState({ value: event.target.value });
  };

  Validate = () => {
    if(this.state.category && this.state.admissionDate && this.state.appointmentDate && this.state.contact && 
      this.state.firstname && this.state.email && this.state.lastname && this.state.dateOfBirth ) {
      var patientObj= {
        category: this.state.category,
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        gender: this.state.value,
        admissionDate: this.state.admissionDate,
        appointmentDate: this.state.appointmentDate,
        contact: this.state.contact,
        email: this.state.email,
        dateOfBirth: this.state.dateOfBirth
      }
      this.props.AddPatient(patientObj);
      window.location.reload();
    }
    else {
      this.handleClickOpen() ;
    }
  }
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render (){
    const { classes } = this.props;
    return (
        <div>
            <Head title={this.state.title} />
            <div className="PatientForm">
              <i class="fa fa-address-book fa-3x"></i>
              <h2>Paitent's Detail</h2>
              <Divider />
              <FormControl className={classes.formControl} style = {{width: '25%',}}>
                <FormHelperText>Select Category</FormHelperText>
                <Select
                  value={this.state.category}
                  onChange={(e) => this.handleChange('category', e)}
                  name="Category"
                  displayEmpty
                  className={classes.selectEmpty}
                >
                  <MenuItem value='Mental Health'>Mental Health</MenuItem>
                  <MenuItem value='MetaBolic And Obacity'>MetaBolic And Obacity</MenuItem>
                  <MenuItem value='Yoga'>Yoga</MenuItem>
                </Select>
              </FormControl>
              <div className="alignField">
                <MuiThemeProvider theme={theme}>
                    <TextField
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    className={classes.margin}
                    onChange = {(e) => this.handleChange('firstname', e)}
                    style = {{width: '25%', marginRight: '5em'}}
                    label="FirstName"
                    value= {this.state.firstname}
                    id="mui-theme-provider-input"
                    />
                </MuiThemeProvider>
                <MuiThemeProvider theme={theme}>
                  <TextField
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    className={classes.margin}
                    onChange = {(e) => this.handleChange('lastname', e)}
                    style = {{width: '25%'}}
                    label="Lastname"
                    value= {this.state.lastname}
                    id="mui-theme-provider-input"
                    />
                </MuiThemeProvider>
              </div>
              <div className="alignField">
                <TextField
                    id="date"
                    label="Date Of Birth"
                    type="date"
                    value= {this.state.dateOfBirth}
                    onChange= {(e) => this.handleChange('dateOfBirth', e)}
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    style = {{width: '25%', marginRight: '4em', marginLeft: '-8em'}}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                />
                <FormControl component="fieldset" required error className={[classes.formControl, 'radioBtn']} >
                    <FormLabel component="label" style ={{color: 'black'}}>Gender</FormLabel>
                    <RadioGroup
                      aria-label="gender"
                      name="gender2"
                      className={classes.group}
                      style ={{display: 'inline'}}
                      value={this.state.value}
                      onChange={(e) => this.radioHandleChange(e)}
                    >
                      <FormControlLabel value="female" control={<Radio color="primary" />} label="Female" />
                      <FormControlLabel value="male" control={<Radio color="primary" />} label="Male" />
                    </RadioGroup>
                </FormControl>
              </div>
              <div className="alignField">
                <MuiThemeProvider theme={theme}>
                    <TextField
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    className={classes.margin}
                    value= {this.state.contact}
                    onChange= {(e) => this.handleChange('contact', e)}
                    style = {{width: '25%', marginRight: '6em'}}
                    label="Contact Number"
                    id="mui-theme-provider-input"
                    type="number"
                    />
                </MuiThemeProvider>
                <TextField
                    id="date"
                    label="Addmission Date"
                    type="date"
                    defaultValue= {this.state.admissionDate}
                    onChange= {(e) => this.handleChange('admissionDate', e)}
                    style = {{width: '25%'}}
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                />
              </div>
              <div className="alignField">
                <TextField
                    id="date"
                    label="Appointment Date"
                    type="date"
                    defaultValue= {this.state.appointmentDate}
                    onChange= {(e) => this.handleChange('appointmentDate', e)}
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    style = {{width: '25%', marginRight: '6em',marginLeft: '1em'}}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                />
                <MuiThemeProvider theme={theme}>
                    <TextField
                    InputProps={{
                      classes: {
                        input: classes.resize,
                      },
                    }}
                    value= {this.state.email}
                    onChange= {(e) => this.handleChange('email', e)}
                    className={classes.margin}
                    style = {{width: '25%',}}
                    label="Email"
                    id="mui-theme-provider-input"
                    type="email"
                    />
                </MuiThemeProvider>
              </div>
              <div className="alignField" style={{paddingBottom: '2em'}}>
              <Button variant="fab" onClick={this.Validate} color="primary" aria-label="add" className={classes.button}>
                <AddIcon />
              </Button>
              </div>
            </div>
            {/* for dialog box */}
            <Dialog
            open={this.state.open}
            TransitionComponent={Transition}
            keepMounted
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
            style={{width: '40%'}}
          >
            <DialogTitle id="alert-dialog-slide-title" style={{color: 'red'}}>
              Warning Alert
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-slide-description">
                Please Fill All Fields
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                OK
              </Button>
            </DialogActions>
          </Dialog>
        </div>
    );
  }
}
AddPatient.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    patient : state.patientReducers 
  }
}

function mapDispatchToProps(dispatch) {
  return {
    AddPatient: function(patientObj){
      return dispatch(Actions.addPatientAct(patientObj));
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(AddPatient));


