import {createStore, applyMiddleware, combineReducers} from 'redux';
import Thunk from 'redux-thunk';
import ActionType from './actions';

// Doctor's reducer 
const initialState = {};
const doctorReducers = function(state = initialState ,action){
    const newState = {...state} ;
    switch(action.type) {
        case ActionType.login : {
            newState.user = action.data;
            break;
        }
        case ActionType.signup : 
        {
            newState.user = action.data;
            break;
        }
        default:
            break;
    }
    return newState;
}

// Patient Reducer
const initalvaue = {};
const patientReducers = function(state = initalvaue, action) {
    const newState = {...state} ;
    switch(action.type) {
        case ActionType.addPatient: {
            newState.patient = action.data;
            break;
        }
        case ActionType.getPatientData: {
            newState.patient = action.data;
            break;
        }
        case ActionType.updatePatient: {
            newState.patient = action.data;
            break;
        }
        case ActionType.deletePatient: {
            newState.patient = action.data;
            break;
        }
        default :
            break ;
    }
    return newState;
}

const allReducers = combineReducers({doctorReducers, patientReducers})

const store = createStore(allReducers , applyMiddleware(Thunk));
export default store ;