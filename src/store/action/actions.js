export default {
    login: 'LOGIN',
    signup: 'SIGNUP',
    addPatient: 'ADD_PATIENT',
    deletePatient: 'DELETE_PATIENT',
    getPatientData: 'GET_PATIENT_DATA',
    updatePatient: 'UPDATE_PATIENT',
}