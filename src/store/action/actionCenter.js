import StoreActions from "./actions";

function doPostRequest({ url, data}) {
    return new Promise( resolve => {
        fetch('http://localhost:4000'+ url, {
            method : 'POST',
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ bodyData: data })
        })
        .then(res => res.json())
        .then(success => {
            resolve(success);
        })
        .catch(err => {
            console.log({ err });
        });
    })
}

function doGetRequest({url}){
    return new Promise(resolve => {
        fetch('http://localhost:4000'+ url)
        .then(res => res.json())
        .then(success => {
            resolve(success);
        })
        .catch(err => {
            console.log({ err });
        });
    })
}

function loginAct(loginData){
    return dispatch => {
        doPostRequest({
            url: '/api/DoctorModule/Login',
            data : loginData
        }).then( success => {
            dispatch ({
                type: StoreActions.login,
                data: success.data
            });
        });
    };
}
function signupAct(signupData) {
    return dispatch => {
        doPostRequest({
            url: '/api/DoctorModule/Signup',
            data : signupData
        }).then( success => {
            dispatch ({
                type: StoreActions.signup,
                data: success.data
            });
        });
    };
}
function addPatientAct(PatientData){
    return dispatch => {
        doPostRequest({
            url: '/api/PatientModule/AddPatient',
            data : PatientData
        }).then( success => {
            dispatch ({
                type: StoreActions.addPatient,
                data: success.data
            });
        });
    };
}

function getAllPatientsDataAct(){
    return dispatch => {
        doGetRequest({url: '/api/PatientModule/GetPatientData'})
        .then (success =>{ 
            let array = [];
            array = success
            dispatch ({
                type: StoreActions.getPatientData,
                data: array.data
            });
        });
    } 
}

function updatePatientAct(PatientData){
    return dispatch => {
        doPostRequest({
            url: `/api/PatientModule/UpdatePatient`,
            data : PatientData
        }).then( success => {
            dispatch ({
                type: StoreActions.updatePatient,
                data: success.data
            });
        });
    }
}

function deletePatientAct(Patient){
    console.log('Action ' ,Patient);
    return dispatch => {
        doPostRequest({
            url: `/api/PatientModule/DeletePatient`,
            data : Patient._id
        }).then( success => {
            dispatch ({
                type: StoreActions.deletePatient,
                data: success.data
            });
        });
    }
}

export default {
    loginAct,
    signupAct,
    addPatientAct,
    deletePatientAct,
    getAllPatientsDataAct,
    updatePatientAct,
}