import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FirstPage from './components/firstPage';
import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';
import Dashboard from './components/Dashboard';
import Head from './components/Head';
import AddPatient from './components/AddPatient';
import {BrowserRouter as Router, Route} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
          <div>
            <Route exact path='/' component={Login} />
            <Route path='/Signup' component={Signup} />
            <Route path='/Dashboard' component={Dashboard} />
            <Route path='/AddPatient' component={AddPatient} />
          </div>
      </Router>
    );
  }
}
export default (App);
